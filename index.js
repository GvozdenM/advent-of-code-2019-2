const globals = {
    puzzelInput: [
        1,12,2,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,19,6,23,2,13,23,27,1,27,13,31,1,9,31,35,1,35,9,39,1,39,5,43,2,6,43,47,1,47,6,51,2,51,9,55,2,55,13,59,1,59,6,63,1,10,63,67,2,67,9,71,2,6,71,75,1,75,5,79,2,79,10,83,1,5,83,87,2,9,87,91,1,5,91,95,2,13,95,99,1,99,10,103,1,103,2,107,1,107,6,0,99,2,14,0,0
    ]
};

const addToken = 1;
const multToken = 2;
const haltToken = 99;

/**
 * @param {Array} opline
 */
const createCommand = (opline) => {
    const inputA = opline[1];
    const inputB = opline[2];
    const output = opline[3];

    const add = function add(programState) {
        programState[output] = programState[inputA] + programState[inputB];
        return programState;
    };

    const mult = function multiply(programState) {
        programState[output] = programState[inputA] * programState[inputB];
        return programState;
    };
    const halt = function () {
        throw new Error("Reached halt instruction! Terminate!")
    };

    if(4 < opline.length) {
        throw new Error("Invalid operation! Failed to parse!");
    }
    else {
        let instruction;
        switch(opline[0]) {
            case addToken:
                instruction = add;
                break;
            case multToken:
                instruction = mult;
                break;
            case haltToken:
                instruction = halt;
                break;
            default:
                throw new Error("Unknown token at position [0] of operational line!");
        }

        return {
            execute: instruction
        }
    }
};


let temp;
let programState = [...globals.puzzelInput];
function interprate(programState){
    var command;
    let localState = [...programState];
    try {
        for(let i = 0;; i += 4) {
            temp = localState.slice(i, i+4);
            command = createCommand(temp);
            localState = command.execute(programState);
        }
    } catch(e) {
        // console.log(e.message);
    }

    return localState;
}

for(let i = 0; i <= 99; i++) {
    for(let j = 0; j <= 99; j++) {
        let editedState = [...programState];
        editedState[1] = i;
        editedState[2] = j;

        if(19690720 === interprate(editedState)[0]) {
            // console.log(interprate(editedState));
            console.log((100 * i) + j);
        }
    }
}
